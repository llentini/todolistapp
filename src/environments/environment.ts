// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCK72ZALB3KpTnv4xs2jbXi9o7-MgAHDno",
    authDomain: "todolistapp-e55ce.firebaseapp.com",
    databaseURL: "https://todolistapp-e55ce.firebaseio.com",
    projectId: "todolistapp-e55ce",
    storageBucket: "todolistapp-e55ce.appspot.com",
    messagingSenderId: "260827482287"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
